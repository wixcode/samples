

// Display Data Collections on a Google Map
import wixData from "wix-data";

$w.onReady(function () {
	//All code for sample here
	let today = new Date();
	var mapLocations = [];
	var mapMarker = [];
	let item;
	wixData.query("Events")
		.gt("eventDate", today)
		.contains("longitude", ".")
		.contains("latitude", ".")
	  	.find()
	  	.then( (results) => {
	    	console.log("Events found: " + results.totalCount);
			for (var i = 0; i < results.totalCount; i++)
			{
				if (typeof results.items[i] !== 'undefined') {
					item = results.items[i];
		  	    	mapMarker = {
		  	    		"id": item._id,
		        		"title": item.title,
		        		"lat": Number(item.latitude),
		        		"lng": Number(item.longitude),
		        		"description": item.title + " AT " + item.venueName
		      		};
					mapLocations.push(mapMarker);
				}
			}
            $w("#map").postMessage(mapLocations);

	  	} )
	  	.catch( (err) => {
	    	let errorMsg = err;
	    	console.log(errorMsg);
	    	console.log("Error: " + item.title);
          } );
          
    // onMessage must always be inside onready to work!      
	$w("#map").onMessage( (event) => {
	  let receivedData = event.data;
	  console.log("Clicked Events id: " + receivedData);
	  
	  wixData.query("Events")
		.eq("_id", receivedData)
	  	.find()
	  	.then( (results) => {
	  		if (results.totalCount > 0) {
		    	console.log("Event found: " + results.totalCount);
		    	let item = results.items[0];
		    	let eventDate = new Date(item.eventDate).toDateString();
		    	
		    	$w("#eventTitle").text = item.title;
		    	//$w("#eventDescription").text = item.description;
		    	$w("#eventDate").text = eventDate;
		    	$w("#eventVenuename").text = item.venueName;
		    	//$w("#eventLink").link = item.eventWebAddress;
		    	//$w("#eventImage").src = item.eventImage;
		    	$w("#detailsBox").show("FadeIn");
	    	
	    	}
		});
	  
    } );
});
