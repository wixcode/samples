// This sample was made by Andreas Kviby of Wixshow.com
import {fetch} from 'wix-fetch';
let secretKey = "YOUR SECRET KEY HERE";

function returnAPIUrl(theImageUrl){
	return "https://api.openalpr.com/v2/recognize_url?image_url="+ theImageUrl + "&secret_key="+secretKey+"&recognize_vehicle=1&country=eu&return_image=0&topn=10";
}
$w.onReady(function () {

});
// This function is a bit buggy on mobile so far
export function getImageUrl(imageSRC)
{
	if (imageSRC.startsWith("wix:image://") || imageSRC.startsWith("image://")){	
		console.log(imageSRC);
		let regexp = /.+?(?=.png)/;
		var newimagesrc = regexp.exec(imageSRC)[0];	
 		let wixImageURL = "https://static.wixstatic.com/media/";	
 		return wixImageURL + newimagesrc + ".png";
 	} else
 	{
 		return imageSRC;
 	}
}
export function scanButton_click(event, $w) {
	console.log(getImageUrl($w("#carImage").src));
	let data = getImageUrl($w("#carImage").src);
	fetch(returnAPIUrl(data), {method: 'post'})
			    .then(response => response.json())
			    .then(async json => { 
					$w("#plateNumber").text = json.results[0].plate;
					$w("#carBrand").text = json.results[0].vehicle.make[0].name;
					$w("#carModel").text = json.results[0].vehicle.make_model[0].name;
					$w("#carYear").text = json.results[0].vehicle.year[0].name;
					$w("#carColor").text = json.results[0].vehicle.color[0].name;
					$w("#carType").text = json.results[0].vehicle.body_type[0].name;
				});
}
export function imageUploadButton_click(event, $w) {

	if ($w("#uploadButton1").value.length > 0) {
		$w("#uploadButton1").startUpload()
      	.then( (uploadedFile) => {
			   $w("#carImage").src = uploadedFile.url;
			   console.log(uploadedFile.url);
	  });
	}
}