# Wix Code Samples
In this repo sample code for videos and articles will be published.

All code published here is provided as is and nobody in the team that publishes code here gives any kind of guarantee that the code works.

Some functions and code are subject to change anytime when Wix releases new versions of the Wix Code platform. We do not update all code samples when new versions of Wix Code is released so always make sure you ready the API docs.

# Links

Wix Code API References
https://www.wix.com/code/reference/

Wix Code Home
https://www.wix.com/code/home

Wix Code Forums
https://www.wix.com/code/home/forum
